# Wikimedia Commons Featured Picture Exif

This is a simplistic scraper and crawler that extracts exif data from featured picture categories on commons and puts them into a sqlite database.


```python
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from sqlalchemy import create_engine
    
engine = create_engine('sqlite:///data/commons.db')
df = pd.read_sql('''
SELECT
    count(*) as photo_count,
    camera_make
FROM photos
WHERE
    camera_make is not null
GROUP BY
    camera_make
HAVING
    photo_count > 25
ORDER BY
    photo_count DESC;
''', engine)

df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>photo_count</th>
      <th>camera_make</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4617</td>
      <td>canon</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2819</td>
      <td>nikon corporation</td>
    </tr>
    <tr>
      <th>2</th>
      <td>714</td>
      <td>sony</td>
    </tr>
    <tr>
      <th>3</th>
      <td>395</td>
      <td>panasonic</td>
    </tr>
    <tr>
      <th>4</th>
      <td>318</td>
      <td>olympus imaging corp.</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.set_index('camera_make')['photo_count'].plot(kind='bar', figsize=(12,10))
```




    <AxesSubplot:xlabel='camera_make'>




![png](README_files/README_2_1.png)


# How many Apple photos are in the set?


```python
df = pd.read_sql('''
SELECT
    count(*) as photo_count,
    CASE camera_make
        WHEN 'apple'
            THEN 'Apple'
        ELSE 'Not Apple'
    END camera_maker
FROM photos
WHERE
    camera_make is not null
GROUP BY
    camera_maker
ORDER BY
    photo_count DESC;
''', engine)

df.set_index('camera_maker')['photo_count'].plot(kind='bar', figsize=(12,10))
```




    <AxesSubplot:xlabel='camera_maker'>




![png](README_files/README_4_1.png)


## Popular Nikon lenses for Animal photos


```python
df = pd.read_sql('''
select count(*) as count, lens
from photos
where lens is not null
    and camera_make like '%nikon%'
    and category like 'Animals%'
group by lens having count > 10
order by count desc;
''', engine)

df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>count</th>
      <th>lens</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>65</td>
      <td>105.0 mm f/2.8</td>
    </tr>
    <tr>
      <th>1</th>
      <td>17</td>
      <td>tamron sp af 90mm f2.8 di macro 1:1 272nii</td>
    </tr>
    <tr>
      <th>2</th>
      <td>15</td>
      <td>150.0 mm f/2.8</td>
    </tr>
    <tr>
      <th>3</th>
      <td>13</td>
      <td>90.0 mm f/2.8</td>
    </tr>
    <tr>
      <th>4</th>
      <td>13</td>
      <td>150.0-600.0 mm f/5.0-6.3</td>
    </tr>
  </tbody>
</table>
</div>




```python

```
