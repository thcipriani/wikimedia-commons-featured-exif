#!/usr/bin/env python3
"""
commons-exif-by-category
========================

Crawl featured picture gallery pages and extract exif data. Insert that
data into a sqlite database.

Copyright: Tyler Cipriani (c) 2021
License: GPLv3
"""

import argparse
import collections
import sys
from urllib.parse import urljoin

from bs4 import BeautifulSoup
import sqlite3
import requests

DB_PATH = 'data/commons.db'
CATEGORIES = {
    'Arachnida': 'Animals/Arthropods/Arachnida',
    'Diptera': 'Animals/Arthropods/Diptera',
    'Hymenoptera': 'Animals/Arthropods/Hymenoptera',
    'Lepidoptera': 'Animals/Arthropods/Lepidoptera',
    'Odonata': 'Animals/Arthropods/Odonata',
    'Other Arthropods': 'Animals/Arthropods',
    'Accipitriformes': 'Animals/Birds/Accipitriformes',
    'Anseriformes': 'Animals/Birds/Anseriformes',
    'Charadriiformes': 'Animals/Birds/Charadriiformes',
    'Passeriformes': 'Animals/Birds/Passeriformes',
    'Pelecaniformes': 'Animals/Birds/Pelecaniformes',
    'Other birds': 'Animals/Birds',
    'Artiodactyla': 'Animals/Mammals/Artiodactyla',
    'Carnivora': 'Animals/Mammals/Carnivora',
    'Other mammals': 'Animals/Mammals',
    'Amphibians': 'Animals/Amphibians',
    'Fish': 'Animals/Fish',
    'Reptiles': 'Animals/Reptiles',
    'Other animals': 'Animals',
    'Bones and fossils': 'Animals/Bones_and_fossils',
    'Shells': 'Animals/Shells',
    'Asparagales': 'Plants/Asparagales',
    'Asterales': 'Plants/Asterales',
    'Other plants': 'Plants',
    'Fungi': 'Fungi',
    'Other lifeforms': 'Other_lifeforms',
    'Natural phenomena': 'Natural_phenomena',
    'Natural scenes': 'Places/Natural',
    'Food and drink': 'Food_and_drink',
    'Historical': 'Historical',
    'Rocks and minerals': 'Objects/Rocks_and_minerals',
    'Sculptures': 'Objects/Sculptures',
    'Architectural elements': 'Objects/Architectural_elements',
    'Other objects': 'Objects',
    'Land vehicles': 'Objects/Vehicles/Land_vehicles',
    'Water transport': 'Objects/Vehicles/Water_transport',
    'Air transport': 'Objects/Vehicles/Air_transport',
    'Space exploration': 'Space_exploration',
    'People': 'People',
    'Portrait': 'People/Portrait',
    'Sports': 'Sports',
    'Agriculture': 'Places/Agriculture',
    'Human settlements': 'Places/Settlements',
    'Natural scenes': 'Places/Natural',
    'Other places': 'Places/Other',
    'Satellite images': 'Places/Satellite_images',
    'Exteriors': 'Places/Architecture/Religious_buildings',
    'Interiors': 'Places/Interiors/Religious_buildings',
    'Ceilings': 'Places/Interiors/Religious_buildings/Ceilings',
    'Exteriors': 'Places/Architecture/Exteriors',
    'Bridges': 'Places/Architecture/Bridges',
    'Castles and fortifications': 'Places/Architecture/Castles_and_fortifications',
    'Cityscapes': 'Places/Architecture/Cityscapes',
    'Industry': 'Places/Industry',
    'Towers': 'Places/Architecture/Towers',
    'Transport': 'Places/Architecture/Transport',
    'Interiors': 'Places/Interiors',
    'Religious buildings interiors': 'Places/Interiors/Religious_buildings',
    'Religious buildings ceilings': 'Places/Interiors/Religious_buildings/Ceilings',
    'Astronomy': 'Astronomy',
    'Satellite images': 'Places/Satellite_images',
    'Space exploration': 'Space_exploration',
}

EXIF = [
    'exif-make',
    'exif-model',
    'exif-lens',
    'exif-fnumber',
    'exif-isospeedratings',
    'exif-exposuretime',
    'exif-focallength',
]


class Photo:
    def __init__(
        self, category, link, make=None, model=None, lens=None,
        fnumber=None, isospeedratings=None, exposuretime=None, focallength=None
    ):
        self.category = category
        self.link = link
        self.make = make
        self.model = model
        self.lens = lens
        self.fnumber = fnumber
        self.iso = isospeedratings
        self.exposure = exposuretime
        self.focal_length = focallength
        if make is None and model is None and lens is None:
            print(link)
        else:
            print(f'{make}\t{model}\t{lens}')


def commons(page):
    return urljoin('https://commons.wikimedia.org', page)


def commons_featured(page):
    return urljoin(commons('/wiki/Commons:Featured_pictures/'), page)


def fetch_page(uri):
    r = requests.get(uri)

    r.raise_for_status()
    soup = BeautifulSoup(r.text, 'html.parser')
    return soup


def parse_exif(row):
    datum = row.find('td')
    if not datum:
        return None
    return datum.text.lower()


def parse_camera_lenses(page, category):
    for box in page.find_all('li', class_='gallerybox'):
        href = commons(box.find_next('a', class_='image')['href'])
        r = requests.get(href)
        r.raise_for_status()
        photo_soup = BeautifulSoup(r.text, 'html.parser')

        exif_data = {}

        for exif in EXIF:
            row = photo_soup.find('tr', class_=exif)
            if row:
                datum = parse_exif(row)
                if datum is not None:
                    exif_data[exif[len('exif-'):]] = datum

        yield Photo(
            category,
            href,
            **exif_data
        )


def parse_args(argsv):
    ap = argparse.ArgumentParser()
    ap.add_argument('-c', '--category', choices=CATEGORIES, action='append')
    return ap.parse_args()


def setup_db():
    """
    setup the database file if it doesn't exist
    """
    conn = sqlite3.connect(DB_PATH)
    # If the db didn't exist at the start of the function, do the setup
    crs = conn.cursor()

    crs.execute('''
        CREATE TABLE IF NOT EXISTS photos (
            id INTEGER PRIMARY KEY,
            category TEXT NOT NULL,
            link TEXT UNIQUE NOT NULL,
            camera_make TEXT,
            camera_model TEXT,
            lens TEXT,
            fnumber TEXT,
            iso TEXT,
            exposure TEXT,
            focal_length TEXT
        );
    ''')
    conn.commit()
    return conn


def main(argsv):
    args = parse_args(argsv)
    conn = setup_db()
    crs = conn.cursor()

    categories = args.category
    if categories is None:
        categories = [*CATEGORIES.keys()]

    for category in categories:
        html = fetch_page(commons_featured(CATEGORIES[category]))
        for photo in parse_camera_lenses(html, CATEGORIES[category]):
            try:
                crs.execute('''
                    INSERT INTO photos(
                        category,
                        link,
                        camera_make,
                        camera_model,
                        lens,
                        fnumber,
                        iso,
                        exposure,
                        focal_length
                    )
                    VALUES(?,?,?,?,?,?,?,?,?)''', (
                        photo.category,
                        photo.link,
                        photo.make,
                        photo.model,
                        photo.lens,
                        photo.fnumber,
                        photo.iso,
                        photo.exposure,
                        photo.focal_length
                    )
                )
                conn.commit()
            except sqlite3.IntegrityError:
                pass


if __name__ == '__main__':
    main(sys.argv)
