#!/usr/bin/env python3
"""
Get featured picture categories
===============================

Crawl the the featured picture categories on Wikimedia commons at
<https://commons.wikimedia.org/wiki/Template:Commons_FP_galleries>
and output links to each featured picture gallery.

Copyright: Tyler Cipriani (c) 2021
License: GPLv3
"""
from bs4 import BeautifulSoup
import requests

r = requests.get('https://commons.wikimedia.org/wiki/Template:Commons_FP_galleries')
r.raise_for_status()
soup = BeautifulSoup(r.text, 'html.parser')

for a in soup.find_all('a'):
    if a.get('href', '').startswith('/wiki/Commons:Featured_pictures/'):
        print("'{}': '{}',".format(a.text, a['href'][len('/wiki/Commons:Featured_pictures/'):]))
