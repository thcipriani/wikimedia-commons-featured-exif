SHELL=/bin/bash

README.md:
	jupyter nbconvert README.ipynb --to markdown --output README.md

categories:
	python3 get-featured-picture-categories.py

database:
	python3 commons-exif-by-category.py

.PHONY: categories database
